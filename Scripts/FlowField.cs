﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Jordan Machalek
 * Generates and manages interation with a 2D flow field of Vector3's
 */
public class FlowField : MonoBehaviour {

    //Attributes
    public Vector3[,] field;
    private const int resoultion = 40;
    private const float roughness = 10f;
    private Vector3 turnVector;

	// Use this for initialization
	void Start ()
    {
        field = new Vector3[resoultion, resoultion];

        //initialize field
        for (int i = 0; i < resoultion; i++)
        {
            for (int j = 0; j < resoultion; j++)
            {
                field[i, j] = Vector3.zero;
            }
        }

        //Generate a flow field based on Perlin Noise
        for (int row = 0; row < resoultion; row++)
        {
            for (int col = 0; col < resoultion; col++)
            {
                //Generate noise value
                float turnAngle = Mathf.PerlinNoise(row / roughness, col / roughness);
                //Scale noise value to an angle and apply to a vector
                turnVector = Quaternion.Euler(0, turnAngle * 360, 0) * Vector3.right;
                //Set vector
                field[row, col] = turnVector;
            }
        }
    }

    //Returns the direction of a flow vector at a specific position - used by vehicle
    public Vector3 GetFlowDirection(Vector3 position)
    {
        //Get X, Z coordinates relative to grid - rounded down
        int xCor = (int)Mathf.Floor(position.x / 5);
        int zCor = (int)Mathf.Floor(position.z / 5);

        //Make sure the coordinates are within bounds
        if(xCor > 39 || xCor < 0 || zCor > 39 || zCor < 0) { return Vector3.zero; }
        
        return field[xCor, zCor];
    }
}
