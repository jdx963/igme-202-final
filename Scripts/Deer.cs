﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Jordan Machalek
 * Handles steering behaviors for a deer in the simulation
 * Exhibits flow field following behavior
 */
public class Deer : Vehicle
{
    //Flow Field Following
    public FlowField ffScript;
    public float fieldWeight;
    private int timer;

    // Use this for initialization
    public override void Start ()
    {
        base.Start();
        
        ffScript = GameObject.Find("SceneManager").GetComponent<FlowField>();
    }

    //Generates movement forces to guide vehicle
    public override void CalcSteeringForces()
    {
        //Reset force
        ultimateForce = Vector3.zero;

        //Keep the vehicle in bounds
        ultimateForce += (StayInBounds() * boundsWeight);

        //Follow flow field
        ultimateForce += (FollowField() * fieldWeight);

        //Avoid obstacles
        ultimateForce += (AvoidObstacle(obstacles) * avoidanceWeight);

        //Limit force
        ultimateForce.Normalize();
        ultimateForce = ultimateForce * maxForce;

        //Apply force to acceleration
        ApplyForce(ultimateForce);
    }

    //Generates movement force based on a flow field
    public Vector3 FollowField()
    {
        //Get the direction of the flow vector at current position
        Vector3 flowVector = ffScript.GetFlowDirection(vehiclePosition);

        //Scale to max speed
        flowVector.Normalize();
        flowVector *= maxSpeed;

        return flowVector - velocity;
    }
}
