﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Jordan Machalek
 * Handles creation and organization of gameobjects in scene
 */
public class SceneManager : MonoBehaviour {

    //Attributes
    public Terrain terrain;
    public GameObject followFront;
    public GameObject followBack;
    public List<GameObject> prefabs; //Holds GameObject prefabs 
    public List<GameObject> obstacleList;
    public GameObject[] fluidArray; //List of objects that have a drag effect
    public List<GameObject> flockList;
    public List<GameObject> waypoints;
    public GameObject flockTarget;
    //public GameObject obstacle;
    private Vector3 avgPos;
    private Vector3 avgRot;

    // Use this for initialization
    void Start ()
    {
        //Get data from terrain
        terrain = Terrain.activeTerrain;

        //Spawn turkeys in the simulation
        MakeTurkeys();

        //Spawn deer in the simulation
        MakeDeer();

        //Save bush objects as fluids
        fluidArray = GameObject.FindGameObjectsWithTag("Fluid");

        //Get all trees as obstacles
        GameObject[] obstacleArray = GameObject.FindGameObjectsWithTag("Tree");
        //Transfer from array to list
        foreach(GameObject ob in obstacleArray)
        {
            obstacleList.Add(ob);
        }

        //Disable Waypoints
        foreach (GameObject wp in waypoints)
        {
            wp.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update()
    {

    }

    //Places a number of turkeys around the world
    private void MakeTurkeys()
    {
        //Get a center point for creating the flock
        Vector3 center = new Vector3(170, 0, 120);

        for (int i = 0; i < 10; i++)
        {
            //Get X and Z values for new flocker
            float xPos = center.x + Random.Range(-20, 20);
            float zPos = center.z + Random.Range(-20, 20);

            //GameObject newFlocker = Instantiate(prefabs[0], new Vector3(xPos, terrain.SampleHeight(new Vector3(xPos, 0, zPos)) + 1, zPos), Quaternion.identity);
            GameObject newFlocker = Instantiate(prefabs[0], new Vector3(xPos, 0.5f, zPos), Quaternion.identity);

            flockList.Add(newFlocker);
        }
    }

    //Places a number of deer around the world
    private void MakeDeer()
    {
        for (int i = 0; i < 6; i++)
        {
            //Get X and Z values for new flocker
            float xPos = Random.Range(40, 190);
            float zPos = Random.Range(40, 190);
            
            Instantiate(prefabs[1], new Vector3(xPos, 0, zPos), Quaternion.identity);
        }
    }

    //Places the hunter in the world and assigns cameras to it - not used in final version
    private void MakeHunter()
    {
        GameObject hunter = Instantiate(prefabs[2], new Vector3(23, 0, 4), Quaternion.Euler(0, 75, 0));

        //Find camera needed and then assign the hunter's transform as its target
        GameObject c1 = GameObject.Find("Hunter_3P_Camera");
        c1.GetComponent<SmoothFollow>().target = hunter.transform;

        GameObject c2 = GameObject.Find("Hunter_FP_Camera");
        c2.GetComponent<SmoothFollow>().target = hunter.transform;
    }
}
