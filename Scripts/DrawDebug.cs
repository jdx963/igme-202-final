﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Jordan Machalek
 * Draws debug lines for a scene
 */
public class DrawDebug : MonoBehaviour {

    //Attributes
    public List<Material> matList;
    public bool drawLines;
    //Flow Field
    private Vector3[,] flowField;
    private const int resoultion = 40;
    //Terrain Information
    Terrain terrain;
    //Path Following
    public List<GameObject> waypoints;
    //Resistance
    public GameObject[] fluidArray;

    // Use this for initialization
    void Start ()
    {
        //Start with debug lines off
        drawLines = false;

        flowField = new Vector3[resoultion, resoultion];

        //initialize field - included for debug purposes, retained for redundancy
        for (int i = 0; i < resoultion; i++)
        {
            for (int j = 0; j < resoultion; j++)
            {
                flowField[i, j] = Vector3.zero;
            }
        }

        //Get flowField vectors
        flowField = GameObject.Find("SceneManager").GetComponent<FlowField>().field;
        //Get waypoints
        waypoints = GameObject.Find("SceneManager").GetComponent<SceneManager>().waypoints;
        //Get resistance objects
        fluidArray = GameObject.Find("SceneManager").GetComponent<SceneManager>().fluidArray;
        //Get terrain information
        terrain = Terrain.activeTerrain;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Toggle debug lines based on input of D key
        if (Input.GetKeyDown(KeyCode.D) && drawLines == false)
        {
            drawLines = true;

            //Make waypoints visible
            foreach (GameObject wp in waypoints)
            {
                wp.SetActive(true);
            }

            //Make resistance areas visible
            foreach (GameObject fl in fluidArray)
            {
                fl.GetComponentInChildren<MeshRenderer>().enabled = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.D) && drawLines == true)
        {
            drawLines = false;

            //Hide Waypoints
            foreach (GameObject wp in waypoints)
            {
                wp.SetActive(false);
            }

            //Hide resistance
            foreach (GameObject fl in fluidArray)
            {
                fl.GetComponentInChildren<MeshRenderer>().enabled = false;
            }
        }
    }

    //Draws debug lines
    public void OnRenderObject()
    {
        if(drawLines == true)
        {
            //Draw Flow Field vectors
            matList[0].SetPass(0);

            for (int row = 0; row < resoultion; row++)
            {
                for(int col = 0; col < resoultion; col++)
                {
                    //Scale row/col # to resoultion ratio
                    int rowX = (row * 5) + 2;
                    int colZ = (col * 5) + 2;
                    float yVal = terrain.SampleHeight(new Vector3(rowX, 0, colZ));

                    Vector3 currVector = new Vector3(rowX, yVal + 5, colZ); // show vector floating above terrian

                    GL.Begin(GL.LINES);
                    GL.Vertex(currVector);
                    GL.Vertex(currVector + (new Vector3(flowField[row, col].x, 0, flowField[row, col].z) * 3));
                    GL.End();
                }
            }
        }
    }
}
