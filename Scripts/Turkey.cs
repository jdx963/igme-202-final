﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Jordan Machalek
 * Handles movement behavior for a turkey in the simulation
 * Exhibits flocking behavior
 */
public class Turkey : Vehicle {

    //Attributes
    public GameObject seekTarget;

    // Use this for initialization
    public override void Start ()
    {
        base.Start();

        companions = sceneManager.GetComponent<SceneManager>().flockList;
    }

    // Update is called once per frame
    public override void Update ()
    {
        base.Update();
    }

    //Generates movement force for the turkey
    public override void CalcSteeringForces()
    {
        //Reset force
        ultimateForce = Vector3.zero;

        //Keep the vehicle in bounds
        ultimateForce += (StayInBounds() * boundsWeight);

        //Avoid companions
        ultimateForce += (Separation() * separationWeight);

        //Alignment
        ultimateForce += (Alignment() * alignWeight);

        //Cohesion
        ultimateForce += (Cohesion() * cohesionWeight);

        //Wander
        ultimateForce += (Wander() * wanderWeight);

        //Avoid obstacles
        ultimateForce += (AvoidObstacle(obstacles) * avoidanceWeight);

        //Limit force
        ultimateForce.Normalize();
        ultimateForce = ultimateForce * maxForce;

        //Apply force to acceleration
        ApplyForce(ultimateForce);
    }
}
